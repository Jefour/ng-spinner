import { Injectable } from '@angular/core';
import {
	ConnectionBackend,
	RequestOptions,
	Request,
	RequestOptionsArgs,
	Response,
	Http,
	Headers
} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoadingService } from './loading.service';

@Injectable()
export class HttpService extends Http {

	constructor(_backend: ConnectionBackend, _defaultOptions: RequestOptions, private loadingService: LoadingService) {
		super(_backend, _defaultOptions);
	}

	get(url: string, options?: RequestOptionsArgs): Observable<Response> {
		this.loading(url, true);
		return super.get(url, options)
			.map((response) => {
				this.loading(url, false);
				return response;
			});
	}

	private loading(url: string, status: boolean): void {
		this.loadingService.loading(url, status);
	}

}
