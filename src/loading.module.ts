import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, Http, RequestOptions, XHRBackend } from '@angular/http';

import { HttpService } from './http.service';
import { LoadingComponent } from './loading.component';
import { LoadingService } from './loading.service';

@NgModule({
	imports: [
		CommonModule,
		HttpModule,
	],
	declarations: [
		LoadingComponent,
	],
	exports: [
		LoadingComponent,
	],
	providers: [
		LoadingService,
		{
			provide: Http,
			useFactory: (xhrBackend: XHRBackend, requestOptions: RequestOptions, loadingService: LoadingService) => {
				return new HttpService(xhrBackend, requestOptions, loadingService);
			},
			deps: [XHRBackend, RequestOptions, LoadingService],
		}
	]
})
export class LoadingModule { }
