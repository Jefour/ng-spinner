import { Component, Input, OnInit } from '@angular/core';
import { LoadingService } from './loading.service';

@Component({
	selector: 'ng-spinner',
	template: `
		<div *ngIf="!loading; else spinner">
			<ng-content></ng-content>
		</div>
		<ng-template #spinner>
			<div class="spinner"></div>
		</ng-template>
	`,
	styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

	public loading = true;
	@Input() name: string;

	constructor(private loadingService: LoadingService) { }

	ngOnInit() {
		this.loadingService.change.subscribe((states) => {
			this.loading = states[this.name];
		});
	}

}
