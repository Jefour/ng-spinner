import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class LoadingService {

	public change: EventEmitter<any> = new EventEmitter();

	constructor() { }

	public loading(stateName: string, stateValue: boolean) {
		const state = new StateHttp();
		state[stateName] = stateValue;
		this.change.emit(state);
	}

}

class StateHttp {
	[key: string]: any;
}
